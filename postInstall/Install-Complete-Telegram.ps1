#Informations about Telegrams API:
#https://core.telegram.org/

#Sets Telegram APIkey and ChatId to Message User on Telegram later
[string]$api = 'API Token from your Bot'
[string]$chatId = 'Your ChatId'

#Gets OSinformations to write into $message
$OSinfo= gcim Win32_OperatingSystem | Select-Object OSArchitecture, Caption;
[string]$message = "$($OSinfo.Caption), $($OSinfo.OSArchitecture) is now succesfully installed on $env:computername"

#Write Message to Telegram User
$restResponse = Invoke-RestMethod -Uri ('https://api.telegram.org/bot{0}/sendMessage?chat_id={1}&text={2}' -f $api, $chatId, $message)