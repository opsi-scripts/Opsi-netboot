#Edit Domain Credentials
$domain = "myDomain"
$password = "myPassword!" | ConvertTo-SecureString -asPlainText -Force
$username = "$domain\myUserAccount" 

#Set Up Credentials Variable
$credential = New-Object System.Management.Automation.PSCredential($username,$password)

#Join the Domain
Add-Computer -DomainName $domain -Credential $credential